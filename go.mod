module gitlab.com/go-load-balancer

go 1.20

require (
	github.com/dboslee/lru v0.0.1
	github.com/google/uuid v1.3.0
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.29.0
	golang.org/x/sync v0.1.0
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	golang.org/x/sys v0.6.0 // indirect
)
