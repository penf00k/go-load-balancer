package log

import (
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"net/http"
	"os"
)

type ZeroLogger struct {
	logger zerolog.Logger
}

func NewZeroLogger() ILogger {
	logger := zerolog.New(os.Stderr).With().Timestamp().Logger()
	return &ZeroLogger{logger: logger}
}

func (l ZeroLogger) Panic(msg string) {
	log.Panic().Msg(msg)
}

func (l ZeroLogger) Fatal(msg string) {
	log.Fatal().Msg(msg)
}

func (l ZeroLogger) Error(err error) {
	log.Error().Err(err)
}

func (l ZeroLogger) Warn(msg string) {
	log.Warn().Msg(msg)
}

func (l ZeroLogger) Info(msg string) {
	log.Info().Msg(msg)
}

func (l ZeroLogger) Debug(msg string) {
	log.Debug().Msg(msg)
}

func (l ZeroLogger) Trace(msg string) {
	log.Trace().Msg(msg)
}

func (l ZeroLogger) Request(req *http.Request) {
	if msg, err := formatRequest(req); err == nil {
		l.Info(msg)
	} else {
		l.Error(errors.Wrap(err, "failed to format request for logging"))
	}
}
