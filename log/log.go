package log

import (
	"bytes"
	"fmt"
	"github.com/pkg/errors"
	"io"
	"net/http"
)

var Logger ILogger

type ILogger interface {
	Panic(msg string)

	Fatal(msg string)

	Error(err error)

	Warn(msg string)

	Info(msg string)

	Debug(msg string)

	Trace(msg string)

	Request(req *http.Request)
}

func formatRequest(req *http.Request) (string, error) {
	var body string
	if req.Body != nil {
		buf, err := io.ReadAll(req.Body)
		if err != nil {
			return "", errors.Wrap(err, "failed to read body")
		}

		req.Body = io.NopCloser(bytes.NewReader(buf))
		body = string(buf)
	}

	return fmt.Sprintf(`new request: path = %s, host = %s, body = %s`, req.URL.Path, req.URL.Host, body), nil
}
