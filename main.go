package main

import (
	"context"
	"fmt"
	"gitlab.com/go-load-balancer/balancer"
	"gitlab.com/go-load-balancer/log"
	"golang.org/x/sync/errgroup"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

// todo di?

/*
config
cache enabled
*/
func main() {
	log.Logger = log.NewZeroLogger()
	log.Logger.Info("starting service")
	lb := balancer.New()
	lb.AddServer("http://localhost:8080")
	lb.AddServer("http://localhost:8081")
	lb.AddServer("http://localhost:8082")
	lb.SetCacheEnabled(true)
	lb.UpdateStrategy("round_robin")
	//lb.UpdateStrategy("random")
	//lb.UpdateStrategy("average_response_time")
	//lb.UpdateStrategy("active_request_count")

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	g, ctx := errgroup.WithContext(ctx)
	g.Go(func() error {
		return lb.Serve(9000)
	})

	g.Go(func() error {
		mux := http.NewServeMux()
		mux.HandleFunc("/addServer", func(writer http.ResponseWriter, request *http.Request) {

		})
		return http.ListenAndServe(fmt.Sprintf(":%d", 9001), mux)
	})

	select {
	case <-interrupt:
		break
	case <-ctx.Done():
		break
	}

	signal.Stop(interrupt)
	cancel()

	err := g.Wait()
	if err != nil {
		log.Logger.Fatal(err.Error())
	}
}
