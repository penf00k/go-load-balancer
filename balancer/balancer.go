package balancer

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
	"sync"
	"time"
)

type Server struct {
	url   *url.URL
	mu    sync.RWMutex
	alive bool
}

func (s *Server) SetAlive(b bool) {
	s.mu.Lock()
	s.alive = b
	s.mu.Unlock()
}

func (s *Server) Alive() bool {
	s.mu.RLock()
	isAlive := s.alive
	s.mu.RUnlock()
	return isAlive
}

func (s *Server) Host() string {
	return s.url.Host
}

type loadBalancer struct {
	servers      []*Server
	currStrategy Strategy
	strategies   map[string]Strategy
	proxy        *httputil.ReverseProxy
	mu           sync.RWMutex
	timeTracker  *timeTracker
	cacheEnabled bool
}

func New() *loadBalancer {
	timeTracker := newTimeTracker(1<<10, 5*time.Second, 5*time.Minute)
	strategies := []Strategy{
		&RoundRobinStrategy{},
		NewRandomStrategy(),
		NewAverageRespTimeStrategy(timeTracker),
		NewActiveRequestCountStrategy(timeTracker),
	}
	strMap := make(map[string]Strategy, len(strategies))
	for _, s := range strategies {
		strMap[s.Name()] = s
	}
	return &loadBalancer{
		strategies:   strMap,
		currStrategy: strategies[0],
		timeTracker:  timeTracker,
	}
}

func (lb *loadBalancer) UpdateStrategy(name string) error {
	for i, s := range lb.strategies {
		if s.Name() == name {
			lb.mu.Lock()
			lb.currStrategy = lb.strategies[i]
			lb.mu.Unlock()
			return nil
		}
	}
	return fmt.Errorf("no strategy '%s' found", name)
}

func (lb *loadBalancer) AvailableStrategyNames() []string {
	lb.mu.RLock()
	defer lb.mu.RUnlock()
	res := make([]string, 0)
	for _, s := range lb.strategies {
		res = append(res, s.Name())
	}
	return res
}

func (lb *loadBalancer) CurrStrategySync() Strategy {
	lb.mu.RLock()
	defer lb.mu.RUnlock()
	return lb.currStrategy
}

func (lb *loadBalancer) AddServer(rawURL string) error {
	u, err := url.Parse(rawURL)
	if err != nil {
		return fmt.Errorf("failed to parse url '%s': %w", rawURL, err)
	}
	lb.mu.Lock()
	defer lb.mu.Unlock()

	var exist bool
	for i := range lb.servers {
		s := lb.servers[i]
		if s.url.String() == u.String() {
			exist = true
			break
		}
	}
	if !exist {
		lb.servers = append(lb.servers, &Server{
			url:   u,
			alive: true,
		})
	}
	return nil
}

func (lb *loadBalancer) RemoveServer(rawURL string) error {
	u, err := url.Parse(rawURL)
	if err != nil {
		return fmt.Errorf("failed to parse url '%s': %w", rawURL, err)
	}
	lb.mu.Lock()
	defer lb.mu.Unlock()

	for i := range lb.servers {
		s := lb.servers[i]
		if s.url.String() == u.String() {
			lb.servers = append(lb.servers[:i], lb.servers[i+1:]...)
		}
	}
	return nil
}

func (lb *loadBalancer) SetCacheEnabled(enabled bool) {
	lb.mu.Lock()
	defer lb.mu.Unlock()
	lb.cacheEnabled = enabled
}

func (lb *loadBalancer) Serve(port uint16) error {
	//go lb.healthCheck() // todo убрать или добавить в серверы ручку

	lb.proxy = &httputil.ReverseProxy{
		Director: lb.director(),
		Transport: newCachingTransport(
			4096,
			newTimeTrackingTransport(
				http.DefaultTransport,
				lb.timeTracker,
				func() bool { return lb.CurrStrategySync().ShouldTrackTime() },
			),
			func() bool { return lb.cacheEnabled },
		),
	}

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", port),
		Handler: lb.proxy,
	}

	return srv.ListenAndServe()
}

func (lb *loadBalancer) director() func(req *http.Request) {
	return func(req *http.Request) {
		server := lb.selectServer()
		// todo всегда сюда попадаем, можно кэш трекать, и если есть значение, то брать любой сервак, потому что все равно потом возьмется из кеша
		if server == nil {
			panic("todo no server found")
		}
		u := server.url
		// любезно позаимствовал из https://github.com/golang/go/blob/master/src/net/http/httputil/reverseproxy.go#L269
		targetQuery := u.RawQuery
		req.URL.Scheme = u.Scheme
		req.URL.Host = u.Host
		req.URL.Path, req.URL.RawPath = joinURLPath(u, req.URL)
		if targetQuery == "" || req.URL.RawQuery == "" {
			req.URL.RawQuery = targetQuery + req.URL.RawQuery
		} else {
			req.URL.RawQuery = targetQuery + "&" + req.URL.RawQuery
		}
		if _, ok := req.Header["User-Agent"]; !ok {
			// explicitly disable User-Agent so it's not set to default value
			req.Header.Set("User-Agent", "")
		}
	}
}

func (lb *loadBalancer) selectServer() *Server {
	lb.mu.RLock()
	defer lb.mu.RUnlock()
	aliveServers := lb.aliveServers()
	if len(aliveServers) == 1 {
		return aliveServers[0]
	}
	for i := 0; i < len(aliveServers); i++ {
		if server := lb.currStrategy.Next(aliveServers); server != nil {
			return server
		}
	}
	return nil
}

func (lb *loadBalancer) aliveServers() []*Server {
	var res []*Server
	for _, s := range lb.servers {
		if s.Alive() {
			res = append(res, s)
		}
	}
	return res
}

func (lb *loadBalancer) healthCheck() {
	for {
		select {
		case <-time.NewTicker(30 * time.Second).C:
			for _, s := range lb.servers {
				s.SetAlive(true)
			}
		}
	}
}

func singleJoiningSlash(a, b string) string {
	aslash := strings.HasSuffix(a, "/")
	bslash := strings.HasPrefix(b, "/")
	switch {
	case aslash && bslash:
		return a + b[1:]
	case !aslash && !bslash:
		return a + "/" + b
	}
	return a + b
}

// спи, жена из https://github.com/golang/go/blob/master/src/net/http/httputil/reverseproxy.go#L209
func joinURLPath(a, b *url.URL) (path, rawpath string) {
	if a.RawPath == "" && b.RawPath == "" {
		return singleJoiningSlash(a.Path, b.Path), ""
	}
	// Same as singleJoiningSlash, but uses EscapedPath to determine
	// whether a slash should be added
	apath := a.EscapedPath()
	bpath := b.EscapedPath()

	aslash := strings.HasSuffix(apath, "/")
	bslash := strings.HasPrefix(bpath, "/")

	switch {
	case aslash && bslash:
		return a.Path + b.Path[1:], apath + bpath[1:]
	case !aslash && !bslash:
		return a.Path + "/" + b.Path, apath + "/" + bpath
	}
	return a.Path + b.Path, apath + bpath
}
