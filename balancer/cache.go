package balancer

import (
	"bufio"
	"bytes"
	"github.com/dboslee/lru"
	"github.com/pkg/errors"
	"gitlab.com/go-load-balancer/log"
	"net/http"
	"net/http/httputil"
)

type reqResp struct {
	req      *http.Request
	respDump []byte
}

type cache interface {
	get(key string) (reqResp, bool)
	set(key string, value reqResp)
}

type lruCache struct {
	cache *lru.SyncCache[string, reqResp]
}

func newLruCache(capacity int) cache {
	return &lruCache{cache: lru.NewSync[string, reqResp](lru.WithCapacity(capacity))}
}

func (l *lruCache) get(key string) (reqResp, bool) {
	return l.cache.Get(key)
}

func (l *lruCache) set(key string, value reqResp) {
	l.cache.Set(key, value)
}

type cachingTransport struct {
	cache     cache
	transport http.RoundTripper
	enabled   func() bool
}

func newCachingTransport(
	capacity int,
	transport http.RoundTripper,
	enabled func() bool,
) *cachingTransport {
	return &cachingTransport{
		cache:     newLruCache(capacity),
		transport: transport,
		enabled:   enabled,
	}
}

func (c *cachingTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	uriKey := req.URL.Path
	if c.enabled() {
		v, ok := c.cache.get(uriKey)
		if ok {
			log.Logger.Trace("returning cached value!")
			reader := bufio.NewReader(bytes.NewReader(v.respDump))
			dumpedResp, err := http.ReadResponse(reader, req)
			if err == nil {
				return dumpedResp, nil
			} else {
				log.Logger.Error(errors.Wrap(err, "failed to ReadResponse for cache"))
			}
		}
	}

	log.Logger.Request(req)
	resp, err := c.transport.RoundTrip(req)
	if err == nil && c.enabled() {
		// todo goroutine?
		respDump, err := httputil.DumpResponse(resp, true)
		if err == nil {
			c.cache.set(uriKey, reqResp{
				req:      req,
				respDump: respDump,
			})
		} else {
			log.Logger.Error(errors.Wrap(err, "failed to DumpResponse for cache"))
		}
	}
	return resp, err
}
