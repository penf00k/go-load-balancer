package balancer

import (
	"math/rand"
	"sync/atomic"
	"time"
)

type Strategy interface {
	Next(servers []*Server) *Server
	Name() string
	ShouldTrackTime() bool
}

type RoundRobinStrategy struct {
	count atomic.Uint32
}

func (s *RoundRobinStrategy) Next(servers []*Server) *Server {
	count := s.count.Add(1)
	return servers[int(count)%len(servers)]
}

func (s *RoundRobinStrategy) Name() string {
	return "round_robin"
}

func (s *RoundRobinStrategy) ShouldTrackTime() bool {
	return false
}

type RandomStrategy struct {
	r *rand.Rand
}

func NewRandomStrategy() *RandomStrategy {
	return &RandomStrategy{
		r: rand.New(rand.NewSource(time.Now().UnixNano())),
	}
}

func (s *RandomStrategy) Next(servers []*Server) *Server {
	min := 0
	max := len(servers)
	idx := s.r.Intn(max-min) + min
	return servers[idx]
}

func (s *RandomStrategy) Name() string {
	return "random"
}

func (s *RandomStrategy) ShouldTrackTime() bool {
	return false
}

type AverageRespTimeStrategy struct {
	timeTracker *timeTracker
}

func NewAverageRespTimeStrategy(timeTracker *timeTracker) *AverageRespTimeStrategy {
	return &AverageRespTimeStrategy{timeTracker: timeTracker}
}

func (s *AverageRespTimeStrategy) Next(servers []*Server) (res *Server) {
	min := s.timeTracker.AverageResponseTime(servers[0].Host())
	res = servers[0]
	for i, server := range servers {
		if art := s.timeTracker.AverageResponseTime(server.Host()); art < min {
			min = art
			res = servers[i]
		}
	}
	return
}

func (s *AverageRespTimeStrategy) Name() string {
	return "average_response_time"
}

func (s *AverageRespTimeStrategy) ShouldTrackTime() bool {
	return true
}

type ActiveRequestCountStrategy struct {
	timeTracker *timeTracker
}

func NewActiveRequestCountStrategy(timeTracker *timeTracker) *ActiveRequestCountStrategy {
	return &ActiveRequestCountStrategy{timeTracker: timeTracker}
}

func (s *ActiveRequestCountStrategy) Next(servers []*Server) (res *Server) {
	min := s.timeTracker.ActiveRequestCount(servers[0].Host())
	res = servers[0]
	for i, server := range servers {
		if arc := s.timeTracker.ActiveRequestCount(server.Host()); arc < min {
			min = arc
			res = servers[i]
		}
	}
	return
}

func (s *ActiveRequestCountStrategy) Name() string {
	return "active_request_count"
}

func (s *ActiveRequestCountStrategy) ShouldTrackTime() bool {
	return true
}
