package balancer

import (
	"github.com/google/uuid"
	"net/http"
	"sync"
	"time"
)

type timeTrackingTransport struct {
	transport   http.RoundTripper
	timeTracker *timeTracker
	shouldTrack func() bool
}

func newTimeTrackingTransport(
	transport http.RoundTripper,
	timeTracker *timeTracker,
	shouldTrack func() bool,
) *timeTrackingTransport {
	return &timeTrackingTransport{
		transport:   transport,
		timeTracker: timeTracker,
		shouldTrack: shouldTrack,
	}
}

func (c *timeTrackingTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	if !c.shouldTrack() {
		return c.transport.RoundTrip(req)
	}

	host := req.URL.Host
	id := uuid.New()
	c.timeTracker.OnStart(host, id, time.Now())
	defer func() { c.timeTracker.OnFinish(host, id, time.Now()) }()
	return c.transport.RoundTrip(req)
}

type timeToIdMap map[uuid.UUID]time.Time
type reqStartsToServerMap map[string]timeToIdMap
type durationToIdMap map[uuid.UUID]time.Duration
type reqDurationsToServerMap map[string]durationToIdMap

type timeTrack struct {
	host string
	id   uuid.UUID
	time time.Time
}

type timeTracker struct {
	startCh           chan timeTrack
	finishCh          chan timeTrack
	averageTimeToHost map[string]time.Duration
	activeCountToHost map[string]int
	mu                *sync.RWMutex
}

func newTimeTracker(bufferSize int, calcFrequency time.Duration, threshold time.Duration) *timeTracker {
	startedReqTime := make(reqStartsToServerMap)
	finishedReqTime := make(reqDurationsToServerMap)
	startCh := make(chan timeTrack, bufferSize)
	finishCh := make(chan timeTrack, bufferSize)
	chmu := sync.RWMutex{}
	mapmu := sync.RWMutex{}

	averageTimeToHost := make(map[string]time.Duration)
	activeCountToHost := make(map[string]int)

	go func() {
		for {
			select {
			case track := <-startCh:
				func() {
					chmu.Lock()
					defer chmu.Unlock()
					sm, ok := startedReqTime[track.host]
					if !ok {
						sm = make(map[uuid.UUID]time.Time)
					}
					sm[track.id] = track.time
					if !ok {
						startedReqTime[track.host] = sm
					}
				}()

			case track := <-finishCh:
				func() {
					chmu.Lock()
					defer chmu.Unlock()
					sm, ok := startedReqTime[track.host]
					if !ok {
						return
					}
					start, ok := sm[track.id]
					if !ok {
						return
					}
					fm, ok := finishedReqTime[track.host]
					if !ok {
						fm = make(map[uuid.UUID]time.Duration)
					}
					fm[track.id] = start.Sub(track.time)
					if !ok {
						finishedReqTime[track.host] = fm
					}
					delete(sm, track.id)
				}()

			case <-time.NewTicker(calcFrequency).C:
				func() {
					chmu.RLock()
					defer mapmu.Unlock()
					mapmu.Lock()
					defer chmu.RUnlock()
					for host, fm := range finishedReqTime {
						var avg time.Duration
						if len(fm) > 0 {
							var sum time.Duration
							for id, d := range fm {
								if d > threshold {
									delete(fm, id)
								} else {
									sum += d
								}
							}
							avg = sum / time.Duration(len(fm))
						}

						averageTimeToHost[host] = avg
					}
					for host, sm := range startedReqTime {
						activeCountToHost[host] = len(sm)
					}
				}()
			}
		}
	}()

	return &timeTracker{
		startCh:           startCh,
		finishCh:          finishCh,
		averageTimeToHost: averageTimeToHost,
		activeCountToHost: activeCountToHost,
		mu:                &mapmu,
	}
}

func (tt timeTracker) OnStart(host string, id uuid.UUID, t time.Time) {
	tt.startCh <- timeTrack{host: host, id: id, time: t}
}

func (tt timeTracker) OnFinish(host string, id uuid.UUID, t time.Time) {
	tt.finishCh <- timeTrack{host: host, id: id, time: t}
}

func (tt timeTracker) AverageResponseTime(host string) time.Duration {
	tt.mu.RLock()
	defer tt.mu.RUnlock()
	return tt.averageTimeToHost[host]
}

func (tt timeTracker) ActiveRequestCount(host string) int {
	tt.mu.RLock()
	defer tt.mu.RUnlock()
	return tt.activeCountToHost[host]
}
